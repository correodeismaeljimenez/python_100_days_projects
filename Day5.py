# 5.1 Nota media
notas = [12, 22, 45, 13, 47, 89, 90, 34, 62, 45, 88]
max = notas[0]

for nota in notas:
    if nota > max:
        max = nota
    else:
        continue
print(max)


# 5.2
## Revisiar el except clause
try: 
    target = input("¿Hasta cuánto quieres sumar, my friend? \n")
    target = int(target)
except:
    target >= 1000
    print("HHHHHHHH")

accumulator = 0
for sumatorio in range(0, target, 2):
    print(sumatorio)
    accumulator += sumatorio

print(accumulator)

##############################################
# 5.3

listaFB = []

for index in range(1, 101):
    print(index)
    if index % 3 == 0 and index % 5 == 0:
        listaFB.append("FizzBuzz")
    elif index % 3 == 0 and index % 5 != 0:
        listaFB.append("Fizz")
    elif index % 5 == 0 and index % 3 != 0:
        listaFB.append("Buzz")
    else:
        listaFB.append(index)

print(listaFB)