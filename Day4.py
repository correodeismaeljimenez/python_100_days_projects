# Treasure map

line1 = ["", "", ""]
line2 = ["", "", ""]
line3 = ["", "", ""]

map = [line1, line2, line3]

position = input("Las coordenadas son: ").lower()

# Ahora hay que transformar el input a coordenas
# Position será un string que contendrá la letra (columna) y número (fila)
columna = ""

if position[0] == "a":
    columna = 0
elif position[0] == "b":
    columna = 1
elif position[0] == "c":
    columna = 2
else:
    exit

fila = int(position[1]) - 1

try:
    map[fila][columna] = "X"
except: 
    (fila > 2 or fila == 0 or columna != "a" and columna != "b" and columna != "c")
    print("Column or Row out of scope.")

print(map)