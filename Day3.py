print("Welcome to the treasure island. Your mission is to find the treasure")
direction = input("You're at a cross road. Do you want to go left or right?. Type 'left' or 'right' ").lower()
if direction == "right":
    print("Game Over.")
elif direction == "left":
    move = input("You come a lake. There is an island in the middle of the lake. Do you want to swim or wait?. Type 'swim' or 'wait' ").lower()
    if move == "swim":
        print("Game Over.")
    elif move == "wait":
        door = input("You arrive at an island unharmed. There are three doors. One red, one yellow and one blue. Which door do you want to open?. Type 'red', 'yellow' or 'blue' ").lower()
        if door == "yellow":
            print("You win!!")
        elif door == "red" or door == "blue":
            print("Game over.")
        else:
            print("Your choice was not an option. Game over.")
            exit
    else:
        print("Your choice was not an option. Game over.")
        exit
else:
    print("Your choice was not an option. Game over.")
    exit