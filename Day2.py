### PROJECT 2 - TIP CALCULATOR
print("Welcome to the tip calculator")
#cuenta
bill = int(input("What was the total bill? €"))
#porcentaje de propina
tipPerc = int(input("How much tip do you wanna give? 10, 12 or 15? "))
#cuenta más propina
finalBill = bill + (tipPerc/100 * bill)
#comensales
numPeople = int(input("How many people to split the bill? "))
#amount per person
individualBill = round((finalBill / numPeople), 2)
print(f"Each person should pay: €{individualBill}")


# Ojito con las conversiones de formatos: input me da un string
# f para print es lo mejor (no tengo que andar convirtiendo)